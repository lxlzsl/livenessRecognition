package com.xxx.livenessRecognition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LivenessRecognitionApplication {

    public static void main(String[] args) {
        SpringApplication.run(LivenessRecognitionApplication.class, args);
    }

}
