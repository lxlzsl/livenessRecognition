package com.xxx.livenessRecognition.controller;

import com.alibaba.fastjson.JSONObject;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.faceid.v20180301.FaceidClient;
import com.tencentcloudapi.faceid.v20180301.models.DetectAuthResponse;
import com.tencentcloudapi.faceid.v20180301.models.GetDetectInfoResponse;
import com.tencentcloudapi.faceid.v20180301.models.LivenessRecognitionRequest;
import com.tencentcloudapi.faceid.v20180301.models.LivenessRecognitionResponse;
import com.tencentcloudapi.ocr.v20181119.models.IDCardOCRResponse;
import com.xxx.livenessRecognition.RequestParams;
import com.xxx.livenessRecognition.config.PicToBase64;
import com.xxx.livenessRecognition.result.JsonResult;
import com.xxx.livenessRecognition.service.TencentCloudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * @Auther: xiangzhongguo
 * @Date: 2020/9/29 17:05
 * @Description:
 */
@Controller
@RequestMapping("livenessRecognition")
public class LivenessRecognitionContorller {

    @Autowired
    TencentCloudService tencentCloudService;


    /**
     * 人脸核身
     *
     * @param requestParam
     * @return
     */
    @RequestMapping("/paramVideo")
    public JsonResult livenessRecognition(@RequestBody RequestParams requestParam) {
        JsonResult jsonResult = new JsonResult();
        if (StringUtils.isEmpty(requestParam.getIdCardNum()) || StringUtils.isEmpty(requestParam.getCardName()) || StringUtils.isEmpty(requestParam.getVideoBase64())) {
            jsonResult.setCode("-1");
            jsonResult.setMsg("参数不能为空");
            return jsonResult;
        }
        try {
//            Credential cred = new Credential("AKIDd2B3ydldlrjZ9U8fbhWxA7sQiiRuo6G9", "lGTikY5KIzi9YgZBV4MN7ZkdZEvKSN0r");
//            HttpProfile httpProfile = new HttpProfile();
//            httpProfile.setEndpoint("faceid.tencentcloudapi.com");
//
//            ClientProfile clientProfile = new ClientProfile();
//            clientProfile.setHttpProfile(httpProfile);
//
//            FaceidClient client = new FaceidClient(cred, "ap-shanghai", clientProfile);

//            LivenessRecognitionRequest req = new LivenessRecognitionRequest();
//            System.out.println("收到请求：证件号:" + requestParam.getIdCardNum() + "  姓名：" + requestParam.getCardName());
//            req.setIdCard(requestParam.getIdCardNum());
//            req.setName(requestParam.getCardName());
//            req.setVideoBase64(requestParam.getVideoBase64().replaceAll("\n|\t", ""));
//            req.setLivenessType("SILENT");

//            LivenessRecognitionResponse resp = client.LivenessRecognition(req);
            LivenessRecognitionResponse resp =
                    (LivenessRecognitionResponse) tencentCloudService.livenessRecognition(requestParam.getCardName(), requestParam.getIdCardNum(), requestParam.getVideoBase64());

            if (resp.getSim() != null && resp.getSim() >= 70.0) {
                jsonResult.setCode("1");
                jsonResult.setMsg("校验成功");
                return jsonResult;
            } else {
                jsonResult.setCode("-1");
                jsonResult.setMsg(resp.getDescription());
                return jsonResult;
            }
        } catch (Exception e) {
            e.printStackTrace();
            jsonResult.setCode("0");
            jsonResult.setMsg("系统异常");
            return jsonResult;
        }

    }


    /**
     * OCR识别证件+人脸核身
     *
     * @param requestParam
     * @return
     */
    @RequestMapping("imageAndVideo")
    @ResponseBody
    public JsonResult livenessRecognition1(@RequestBody RequestParams requestParam) {
        JsonResult jsonResult = new JsonResult();
        if (StringUtils.isEmpty(requestParam.getImageBase64()) || StringUtils.isEmpty(requestParam.getVideoBase64())) {
            jsonResult.setCode("-1");
            jsonResult.setMsg("参数不能为空");
            return jsonResult;
        }
        try {
            //OCR识别出姓名和证件号
            IDCardOCRResponse idCardOCRResponse = (IDCardOCRResponse) tencentCloudService.IDCardOCR(requestParam.getImageBase64());
            if (idCardOCRResponse == null) {
                jsonResult.setCode("-1");
                jsonResult.setMsg("身份证识别错误");
                return jsonResult;
            }
            LivenessRecognitionResponse livenessRecognitionResponse =
                    (LivenessRecognitionResponse) tencentCloudService.livenessRecognition(idCardOCRResponse.getName(), idCardOCRResponse.getIdNum(), requestParam.getVideoBase64());
            if (livenessRecognitionResponse.getSim() != null && livenessRecognitionResponse.getSim() >= 70.0) {
                jsonResult.setCode("1");
                jsonResult.setMsg("校验成功");
                jsonResult.setData(idCardOCRResponse);
                return jsonResult;
            } else {
                jsonResult.setCode("-1");
                jsonResult.setMsg(livenessRecognitionResponse.getDescription());
                return jsonResult;
            }
        } catch (Exception e) {
            e.printStackTrace();
            jsonResult.setCode("0");
            jsonResult.setMsg("系统异常");
            return jsonResult;
        }

    }



    @RequestMapping("detectAuth")
    public String detectAuth() {
        DetectAuthResponse detectAuthResponse = (DetectAuthResponse) tencentCloudService.detectAuth();
        return "redirect:"+detectAuthResponse.getUrl();

    }


    @RequestMapping("getDetectAuth")
    @ResponseBody
    public String getDetectAuth(String BizToken) {
        GetDetectInfoResponse detectAuthResponse = (GetDetectInfoResponse) tencentCloudService.getDetectAuth(BizToken);
        JSONObject jsonObject=JSONObject.parseObject(detectAuthResponse.getDetectInfo());
        JSONObject jsonObjectText=JSONObject.parseObject(jsonObject.get("Text").toString());
        JSONObject jsonObjectIdCardData=JSONObject.parseObject(jsonObject.get("IdCardData").toString());
        JSONObject jsonObjectVideoData=JSONObject.parseObject(jsonObject.get("VideoData").toString());

        //本次核身最终结果。0为成功
        String ErrCode=jsonObjectText.get("ErrCode").toString();
        //ocr阶段获取的身份证号
        String OcrIdCard=jsonObjectText.get("OcrIdCard")==null?"":jsonObjectText.get("OcrIdCard").toString();
        //ocr阶段获取的姓名
        String OcrName=jsonObjectText.get("OcrName")==null?"":jsonObjectText.get("OcrName").toString();
        // ocr阶段获取的地址
        String OcrAddress=jsonObjectText.get("OcrAddress")==null?"":jsonObjectText.get("OcrAddress").toString();
        //ocr阶段获取的出生信息
        String OcrBirth=jsonObjectText.get("OcrBirth")==null?"":jsonObjectText.get("OcrBirth").toString();
        //ocr阶段获取的性别
        String OcrGender=jsonObjectText.get("OcrGender")==null?"":jsonObjectText.get("OcrGender").toString();
        //ocr身份证正面base64
        String OcrFront=jsonObjectIdCardData.get("OcrFront")==null?"":jsonObjectIdCardData.get("OcrFront").toString();
        //ocr身份证反面base64
        String OcrBack=jsonObjectIdCardData.get("OcrBack")==null?"":jsonObjectIdCardData.get("OcrBack").toString();
        //活体视频base64
        String LivenessVideo=jsonObjectVideoData.get("LivenessVideo")==null?"":jsonObjectVideoData.get("LivenessVideo").toString();


        //url填写要跳转的url
        String url="";
        return "redirect:"+url+"/?ErrCode="+ErrCode+"&OcrIdCard="+OcrIdCard+"&OcrName="+OcrName
                +"&OcrAddress="+OcrAddress+"&OcrBirth="+OcrBirth+"&OcrGender="+OcrGender
                +"&OcrFront="+OcrFront+"&OcrBack="+OcrBack+"&LivenessVideo="+LivenessVideo;

    }

    /**
     * @author: 梁晓檑
     * description: 身份证图片识别
     * create time: 2020-11-9 14:28
     * @Param
     * @return
     */
    @ResponseBody
    @PostMapping("/idCard")
    public JsonResult idCard(@RequestBody RequestParams requestParam){
        JsonResult jsonResult = new JsonResult();
        String imageBase64 = requestParam.getImageBase64();
        if (StringUtils.isEmpty(imageBase64)) {
            jsonResult.setCode("-1");
            jsonResult.setMsg("参数不能为空");
            return jsonResult;
        }
        //OCR识别出姓名和证件号
        IDCardOCRResponse idCardOCRResponse = (IDCardOCRResponse) tencentCloudService.IDCardOCR(imageBase64);
        if (idCardOCRResponse == null) {
            jsonResult.setCode("-1");
            jsonResult.setMsg("身份证识别错误");
            return jsonResult;
        }
        jsonResult.setCode("1");
        jsonResult.setCode("识别成功");
        jsonResult.setData(idCardOCRResponse);
        return jsonResult;
    }

    @RequestMapping("/test")
    public JsonResult test(){
        String imageBase64 = PicToBase64.ImageToBase64("E:\\1.jpg");
        IDCardOCRResponse idCardOCRResponse = (IDCardOCRResponse) tencentCloudService
                .IDCardOCR(imageBase64);
        System.out.println(idCardOCRResponse);

        JsonResult jsonResult = new JsonResult();
        jsonResult.setData(idCardOCRResponse);
        jsonResult.setCode("1");
        return jsonResult;

//        String videoBase64 = PicToBase64.ImageToBase64("C:\\Users\\lxl\\Desktop\\img\\3.mp4");
//        LivenessRecognitionResponse livenessRecognitionResponse =
//                (LivenessRecognitionResponse) tencentCloudService.livenessRecognition(idCardOCRResponse.getName(), idCardOCRResponse.getIdNum(), videoBase64);
//        if (livenessRecognitionResponse.getSim() != null && livenessRecognitionResponse.getSim() >= 70.0) {
//            jsonResult.setCode("1");
//            jsonResult.setMsg("校验成功");
//            jsonResult.setData(idCardOCRResponse);
//            return jsonResult;
//        } else {
//            jsonResult.setCode("-1");
//            jsonResult.setMsg(livenessRecognitionResponse.getDescription());
//            return jsonResult;
//        }
    }

    @RequestMapping("/test1")
    public String test1(){
        return "livenessRecognition-wechat";
    }

    @GetMapping("/identificationView")
    public String identificationView(Model model){
        model.addAttribute("userID", "iierehgekshlg");
        model.addAttribute("userName", "梁小蕾");
        return "identification";
    }

}
