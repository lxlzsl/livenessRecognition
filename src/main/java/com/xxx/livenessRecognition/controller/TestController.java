package com.xxx.livenessRecognition.controller;

import com.xxx.livenessRecognition.util.AccessToken;
import com.xxx.livenessRecognition.util.Sign;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author ：梁晓檑
 * @date ：Created in 2020-12-2 16:05
 * @description：测试
 * @modified By：
 */
@RestController
@RequestMapping("/test")
public class TestController {

    /**
     * 数据库用户名和密码
     * user = 'dz_qinmin';
     * pwd = 'RTBReRNhSYtFXPEA';
     *
     */

    @GetMapping("test1")
    public Map<String, String> test1(String url){
        Map<String, String> ret = Sign.sign(AccessToken.getTicket(), url);
        return ret;
    }
}
