//package com.xxx.livenessRecognition.util;
//
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//
///**
// *
// * @Description: 定时获取微信accessToken（7100s）保存到内存中
// * @Create
// * @Version: V1.00
// * @Author:
// */
////@Component
//public class WeixinAccessTokenTask {
//
//    // 第一次延迟1秒执行，当执行完后7100秒再执行
//    @Scheduled(initialDelay = 1000, fixedDelay = 7000 * 1000)
//    public void getWeixinAccessToken() {
//        try {
//            AccessToken.tokenMap.put("token",AccessToken.getAccessToken());
//            System.out.println("获取到的微信accessToken为" + AccessToken.tokenMap.get("token"));
//        } catch (Exception e) {
//            System.out.println("获取微信adcessToken出错，信息如下");
//            e.printStackTrace();
//        }
//
//    }
//
//
//}