package com.xxx.livenessRecognition.util;

import com.alibaba.fastjson.JSONObject;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.TransferManager;

import java.io.File;
import java.util.List;

/**
 * @author ：梁晓檑
 * @date ：Created in 2020-12-3 14:33
 * @description：对象存储
 * @modified By：
 */
public class FileUtil {
    private static final String SECRETID = "AKIDvd5ILB68G8l7E7g7GArOgjY8TZP1sWO7";
    private static final String SECRETKEY = "f5FQOn2FH5e3Gj01mHZcR1MTacQzKRJv";
    private static final String BUCKETNAME = "lxl-1253671400";
    private static final String REGION = "ap-chengdu";
    public static COSClient cosClient = null;

    static {
        COSCredentials cred = new BasicCOSCredentials(SECRETID, SECRETKEY);
        // 2 设置bucket的区域,
        ClientConfig clientConfig = new ClientConfig(new Region(REGION));
        cosClient = new COSClient(cred, clientConfig);
    }

    public static void uploadFile(){
        // 指定要上传的文件
        File localFile = new File("E:\\1.png");

        // 指定要上传到 COS 上对象键
        String key = "exampleobject2";
        PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKETNAME, key, localFile);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        System.out.println(JSONObject.toJSONString(putObjectRequest));
    }

    public static void download(){
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest();
        // 设置bucket名称
        listObjectsRequest.setBucketName(BUCKETNAME);
        // prefix表示列出的object的key以prefix开始
        listObjectsRequest.setPrefix("example");
        // deliter表示分隔符, 设置为/表示列出当前目录下的object, 设置为空表示列出所有的object
        listObjectsRequest.setDelimiter("/");
        // 设置最大遍历出多少个对象, 一次listobject最大支持1000
        listObjectsRequest.setMaxKeys(1000);
        ObjectListing objectListing = null;
        do {
            try {
                objectListing = cosClient.listObjects(listObjectsRequest);
            } catch (CosServiceException e) {
                e.printStackTrace();
                return;
            } catch (CosClientException e) {
                e.printStackTrace();
                return;
            }
            // common prefix表示表示被delimiter截断的路径, 如delimter设置为/, common prefix则表示所有子目录的路径
            List<String> commonPrefixs = objectListing.getCommonPrefixes();

            // object summary表示所有列出的object列表
            List<COSObjectSummary> cosObjectSummaries = objectListing.getObjectSummaries();
            for (COSObjectSummary cosObjectSummary : cosObjectSummaries) {
                // 文件的路径key
                String key = cosObjectSummary.getKey();
                // 文件的etag
                String etag = cosObjectSummary.getETag();
                // 文件的长度
                long fileSize = cosObjectSummary.getSize();
                // 文件的存储类型
                String storageClasses = cosObjectSummary.getStorageClass();
            }

            String nextMarker = objectListing.getNextMarker();
            listObjectsRequest.setMarker(nextMarker);
        } while (objectListing.isTruncated());
    }

    public static void getFile(){
        ObjectMetadata objectMetadata = cosClient.getObjectMetadata(BUCKETNAME, "exampleobject2");
        System.out.println(objectMetadata);
    }

    public static void main(String[] args) {
        FileUtil.getFile();
    }
}
