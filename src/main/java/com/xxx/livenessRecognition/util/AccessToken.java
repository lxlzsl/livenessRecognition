package com.xxx.livenessRecognition.util;

import com.alibaba.fastjson.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：梁晓檑
 * @date ：Created in 2020-12-2 15:25
 * @description：获取token
 * @modified By：
 */
public class AccessToken {
    private static final String APPID = "wx1c321549f2e3cb1d";
    private static final String APPSECRET = "4c3086c8bec9bb8041dc5443414d47b7";

    public static Map<String,String> tokenMap = new HashMap<>();

    public static String getAccessToken() throws Exception {
        String accessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
                + APPID + "&secret=" + APPSECRET;
        System.out.println("URL for getting accessToken accessTokenUrl=" + accessTokenUrl);

        URL url = new URL(accessTokenUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.connect();

        //获取返回的字符
        InputStream inputStream = connection.getInputStream();
        int size = inputStream.available();
        byte[] bs = new byte[size];
        inputStream.read(bs);
        String message = new String(bs, "UTF-8");

        //获取access_token
        JSONObject jsonObject = JSONObject.parseObject(message);
        String accessToken = jsonObject.getString("access_token");
        String expires_in = jsonObject.getString("expires_in");
        System.out.println("accessToken=" + accessToken);
        System.out.println("expires_in=" + expires_in);
        return accessToken;
    }

    public static String getTicket(){
        try {
            String ticketUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+AccessToken.tokenMap.get("token")+"&type=jsapi";
            System.out.println("ticketUrl=" + ticketUrl);

            URL url = new URL(ticketUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.connect();

            //获取返回的字符
            InputStream inputStream = connection.getInputStream();
            int size = inputStream.available();
            byte[] bs = new byte[size];
            inputStream.read(bs);
            String message = new String(bs, "UTF-8");

            //获取ticket
            JSONObject jsonObject = JSONObject.parseObject(message);
            String ticket = jsonObject.getString("ticket");
            System.out.println("ticket=" + ticket);
            return ticket;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        try {
            AccessToken.getAccessToken();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
