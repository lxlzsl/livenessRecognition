package com.xxx.livenessRecognition.service;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.faceid.v20180301.FaceidClient;
import com.tencentcloudapi.faceid.v20180301.models.*;
import com.tencentcloudapi.ocr.v20181119.OcrClient;
import com.tencentcloudapi.ocr.v20181119.models.IDCardOCRRequest;
import com.tencentcloudapi.ocr.v20181119.models.IDCardOCRResponse;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

/**
 * @Auther: xiangzhongguo
 * @Date: 2020/9/29 19:27
 * @Description:
 */
@Service
public class TencentCloudService {


    private static final String secretId = "AKIDd2B3ydldlrjZ9U8fbhWxA7sQiiRuo6G9";
    private static final String secretKey = "lGTikY5KIzi9YgZBV4MN7ZkdZEvKSN0r";


    /**
     * 身份证ORC识别
     *
     * @param imageBase64 身份证图片base64
     * @return
     */
    @SneakyThrows
    public Object IDCardOCR(String imageBase64) {
        try{
            Credential cred = new Credential(secretId, secretKey);

            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("ocr.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);

            OcrClient client = new OcrClient(cred, "ap-shanghai", clientProfile);

            IDCardOCRRequest req = new IDCardOCRRequest();
            req.setImageBase64(imageBase64);

            IDCardOCRResponse resp = client.IDCardOCR(req);
            return resp;
        }catch (Exception e){
            return null;
        }
    }


    /**
     * 人脸核身
     *
     * @param cardName    姓名
     * @param idCardNum   证件号
     * @param videoBase64 视频的base64
     * @return
     */
    @SneakyThrows
    public Object livenessRecognition(String cardName, String idCardNum, String videoBase64) {
        try{
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("faceid.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);

            FaceidClient client = new FaceidClient(cred, "ap-shanghai", clientProfile);

            LivenessRecognitionRequest req = new LivenessRecognitionRequest();
            req.setIdCard(idCardNum);
            req.setName(cardName);
            req.setVideoBase64(videoBase64.replaceAll("\n|\t", ""));
            req.setLivenessType("SILENT");

            LivenessRecognitionResponse resp = client.LivenessRecognition(req);
            return resp;
        }catch (Exception e){
            return null;
        }
    }


    /**
     * 人脸核身链接入口
     * @return
     */
    @SneakyThrows
    public Object detectAuth( ) {
        Credential cred = new Credential(secretId, secretKey);

        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint("faceid.tencentcloudapi.com");

        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);

        FaceidClient client = new FaceidClient(cred, "ap-shanghai", clientProfile);

        DetectAuthRequest req = new DetectAuthRequest();

        req.setRedirectUrl("http://weixin-green1.natapp1.cc/livenessRecognition/getDetectAuth");
        req.setRuleId("1");

        DetectAuthResponse resp = client.DetectAuth(req);
        return resp;


    }


    /**
     * 获取人脸核身验证结果
     * @param bizToken
     * @return
     */
    @SneakyThrows
    public Object getDetectAuth(String bizToken ) {
        Credential cred = new Credential(secretId, secretKey);

        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint("faceid.tencentcloudapi.com");

        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);

        FaceidClient client = new FaceidClient(cred, "ap-shanghai", clientProfile);

        GetDetectInfoRequest req = new GetDetectInfoRequest();
        req.setBizToken(bizToken);
        req.setRuleId("1");

        GetDetectInfoResponse resp = client.GetDetectInfo(req);

        return resp;
    }




}
