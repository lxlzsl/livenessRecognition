package com.xxx.livenessRecognition;

import lombok.Data;

/**
 * @Auther: xiangzhongguo
 * @Date: 2020/9/29 17:25
 * @Description:
 */
@Data
public class RequestParams {

    String idCardNum;

    String cardName;

    String videoBase64;

    String imageBase64;
}
