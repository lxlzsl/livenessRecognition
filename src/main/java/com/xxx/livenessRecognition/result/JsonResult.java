package com.xxx.livenessRecognition.result;

import lombok.Data;

/**
 * @Auther: xiangzhongguo
 * @Date: 2020/9/29 17:09
 * @Description:
 */
@Data
public class JsonResult {

    String msg;

    String code;

    Object data;
}
